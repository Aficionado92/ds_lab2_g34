added classes:				modified classes:
TCPListenerThread			Chatserver
UDPListenerThread			Client
ShellUsable
ClientListener




Summary:

At the beginning of Chatserver as well as Client starts the instance of each class respectively
in a new Thread. In addition in Chatserver is also created a list of users, that will be used in
course of the app.
For processing of commands I use the prepared Shell implementation, with information that are needed by
the user sent on a given userResponseStream.


Chatserver:

Server first creates the ServerSocket and DatagramSocket, with given parameters from Config-file.
Then server listens on this ServerSocket for whole duration of run of the application and creates
new Thread for every Client application. In case that any error occurs by server-client communication
or one side closes the socket is Thread interrupted.
For handling of TCP/UDP connections I used TCP/UDP-ListenerThread classes, where the messages from
clients are processed. To every command sent to server I add some additional information that are
needed to proper function of server f.E. to check if user is logged in, I send at all occasions
componentName. This componentName is updated every time, that user successfully logs in.
For reducing of overheat of server I used Thread pool with maximum size of 10 Threads. Every time a
new client connects is this Thread also added to ArrayList<> clientThreads, which is in my
implementation important by sending of public messages.
To store informations about who is logged in I use ArrayList<>. Into this list I add in TCPListenerthread
each user, who successfully log in trough using a !login command.
To store registration informations, or in other words information from !register command, I used HashMap
with Key being username and Value >>IP:port<< which I later split at >>:<< where it is needed.


Client:

Client first creates Socket and binds him with given address from Config-file. Then he creates
DatagramSocket. After these steps client listens for incomping messages from server for a whole
duration of run of the app. These messages are immediately sent to userResponseStream, and in some
cases used as a status changing situations.
By the !register command I first check if the given private address correct format has and if it is so,
the privateSocket is initialized with these informations and a new Thread is started for this address.
When user logs out is this socket closed and Thread interrupted.
Also in my implementation, when the server goes down is client also forced to shut down.