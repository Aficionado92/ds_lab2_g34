package chatserver;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.net.Socket;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.security.Key;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import cli.Command;
import cli.ShellUsable;
import nameserver.INameserverForChatserver;
import nameserver.exceptions.AlreadyRegisteredException;
import nameserver.exceptions.InvalidDomainException;
import util.Config;
import util.Keys;

public class Chatserver implements IChatserverCli, Runnable {

    private String componentName;
    private Config config;
    private InputStream userRequestStream;
    private PrintStream userResponseStream;

    private static Config userConfig;
    private static ArrayList<String> userList = new ArrayList<>();
    private static ArrayList<String> loggedInUsers = new ArrayList<>();

    private ServerSocket TCPSocket = null;
    private DatagramSocket UDPSocket = null;
    private ArrayList<TCPListenerThread> clientThreads = new ArrayList<>();
    private UDPListenerThread UDPThread = null;
    private ShellUsable shell;
    private HashMap<String, String> registeredUsers = new HashMap<>();
    private ExecutorService threadPool = Executors.newFixedThreadPool(10);
    private Key privateKey = null;

	private boolean quit = false;

    /**
     * NameServer variables
     */
    INameserverForChatserver rootNS;

    /**
     * @param componentName
     *            the name of the component - represented in the prompt
     * @param config
     *            the configuration to use
     * @param userRequestStream
     *            the input stream to read user input from
     * @param userResponseStream
     *            the output stream to write the console output to
     */
    public Chatserver(String componentName, Config config, InputStream userRequestStream, PrintStream userResponseStream) {
        this.componentName = componentName;
        this.config = config;
        this.userRequestStream = userRequestStream;
        this.userResponseStream = userResponseStream;
        userConfig = new Config("user");
        this.shell = new ShellUsable(componentName, this, userRequestStream, userResponseStream);
        String path = config.getString("key");
        File key = new File(path);
        try {
			privateKey = Keys.readPrivatePEM(key);
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    public void loadRootNameServer(){
    	Registry registry;
		try {
			registry = LocateRegistry.getRegistry(config.getString("registry.host"), config.getInt("registry.port"));
			
			rootNS = (INameserverForChatserver) registry.lookup(config.getString("root_id"));
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (NotBoundException e) {
			e.printStackTrace();
		}
		
		//System.out.println("Rootserver loaded from chatServer!");
	}
    
    public INameserverForChatserver getRootNS() {
		return rootNS;
	}

    @Override
    public void run() {
        this.threadPool.execute(this.shell);
        this.loadRootNameServer();
        
        try {
            this.TCPSocket = new ServerSocket(this.config.getInt("tcp.port"));
        } catch (IOException e) {
            throw new RuntimeException("Cannot listen on TCP port.", e);
        }

        try {
            this.UDPSocket = new DatagramSocket(this.config.getInt("udp.port"));
            this.UDPThread = new UDPListenerThread(this.UDPSocket, this);
            new Thread(this.UDPThread).start();
        } catch(IOException e) {
            throw new RuntimeException("Cannot listen on UDP port.", e);
        }

        while(!this.quit) {
            try {
                Socket s = this.TCPSocket.accept();
                TCPListenerThread th = new TCPListenerThread(s, this);
                this.clientThreads.add(th);
                this.threadPool.execute(th);
            } catch(IOException e){}
        }
    }

    @Override
    @Command
    public String users() throws IOException {
        Iterator<String> it = userList.iterator();
        String users = "";
        for(int i = 1; i <= userList.size(); i++) {
            String u = it.next();
            if(loggedInUsers.contains(u)) {
                users = users + (i + ". " + u + " online\n");
            } else {
                users = users + (i + ". " + u + " offline\n");
            }
        }
	    this.userResponseStream.println(users);
        return null;
    }

    @Override
    @Command
    public String exit() throws IOException {
        this.threadPool.shutdown();
        try {
            this.TCPSocket.close();
            this.UDPSocket.close();
        } catch(IOException e) {
        }
        for (TCPListenerThread t : this.clientThreads) {
            t.stop();
        }
        this.UDPThread.stop();
        this.shell.getShell().close();
        this.quit = true;
        return "Server is being terminated.\n";
    }

    /**
     * @param args
     *            the first argument is the name of the {@link Chatserver}
     *            component
     */
    public static void main(String[] args) {
        Chatserver chatserver = new Chatserver(args[0], new Config("chatserver"), System.in, System.out);
        createUserList();
        new Thread(chatserver).start();
    }


    /*-----------------------------------------------------------------------------------------------------------------
    -----------------------------------------------------------------------------------------------------------------*/

    private static void createUserList() {
        Set<String> uKeys = userConfig.listKeys();
        for(Iterator<String> it = uKeys.iterator(); it.hasNext(); ) {
            String s = it.next();
            s = s.replace(".password","");
            userList.add(s);
        }
        Collections.sort(userList);
    }

    public Config getConfig() {
        return this.config;
    }

    public Key getPrivateKey() {
        return this.privateKey;
    }

    public ArrayList<String> getLoggedInUsers() {
        return loggedInUsers;
    }

    public void setLoggedInUser(String user) {
        loggedInUsers.add(user);
    }

    public void removeLoggedInUser(String user) {
        loggedInUsers.remove(user);
    }

    public Config getUserConfig() {
        return userConfig;
    }

    public String setRegisteredUser(String user, String address) {
    	try {
			this.rootNS.registerUser(user, address);
			return "Successfully registered address for "+user+".";
		} catch (RemoteException e) {
			return e.getMessage();
		} catch (AlreadyRegisteredException e) {
			return e.getMessage();
		} catch (InvalidDomainException e) {
			return e.getMessage();
		}
    }

    public void removeRegisteredUser(String user) {
        this.registeredUsers.remove(user);
    }

    public boolean isRegistered(String user) {
        return this.registeredUsers.containsKey(user);
    }

    public void publicMessage() {
        ArrayList<TCPListenerThread> helpList = new ArrayList<>(this.clientThreads);
        for (TCPListenerThread t : helpList) {
            if (!t.getClientName().equals(t.getSender())) {
                if(!t.getClientName().equals("")) {
                    if(!t.getSender().equals("")) {
                        t.publicMessage();
                    }
                }
            }
        }
    }

}
