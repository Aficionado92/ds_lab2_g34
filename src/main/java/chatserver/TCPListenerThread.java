package chatserver;

import org.bouncycastle.util.encoders.Base64;
import util.Config;

import java.io.*;
import java.net.Socket;
import java.rmi.RemoteException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;

import nameserver.INameserverForChatserver;
import util.Keys;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;

public class TCPListenerThread implements Runnable {

	private static SecureRandom secureRandom = new SecureRandom();

	private Socket socket;
	private Chatserver server;
	private String clientName = "";
	private boolean exit = false;
	private boolean auth = false;
	private BufferedReader reader = null;
	private PrintWriter writer = null;

	private static String publicMsg = "";
	private static String sender = "";
	private static int i = 0;
	private int authCounter=0;
	private Cipher encipher;
	private Cipher decipher;
	byte[] serverChallenge;

	public TCPListenerThread(Socket serverSocket, Chatserver server) {
		this.socket = serverSocket;
		this.server = server;
	}

	public void run() {

		while (true) {
			try {
				this.reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				this.writer = new PrintWriter(socket.getOutputStream(), true);

				String request;

				// read client requests and respond to them
				while (!exit) {
					if((request = this.reader.readLine()) != null) {
						String response = "ERROR: Provided message does not fit the expected format.";
						String msg="";
						if(!auth) {
							if(authCounter==1){
								byte[] servCh=null;
								if(serverChallenge!=null){
									servCh=Base64.encode(serverChallenge);
								}
					        	if((servCh!=null) && Arrays.toString(decryptMsg(request)).equals(Arrays.toString(servCh))){
					        		auth=true;
					        		//user einloggen
					        		response = login(this.clientName);
					        	}else{
					        		//stop handshake 
					        		response = "!Failed to authenticate!";
					        		exit=true;
					        	}
							}else{
								response = decryptAuthMsg(request);
							}

						} else {
							//decript the msg
							msg = new String(decryptMsg(request), "UTF-8");
						}
						String[] parts = msg.split("\\s");
						
						switch (parts[0]) {
						    case "!logout":
							    response = logout(parts);
							    break;
						    case "!send":
							    response = send(parts);
							    break;
						    case "!register":
							    response = register(parts);
							    break;
						    case "!lookup":
							    response = lookup(parts);
							    response = "!lookup"+ response;
							    break;
						    default:
							    break;
						}
						
						if(auth){
							writer.println(new String(encryptMsg(response), "UTF-8"));
						}else{
							writer.println(response);
						}
					}
				}

			} catch (IOException e) {
				this.exit = true;
				break;
			} finally {
				if (socket != null && !socket.isClosed()) {
					try {
						socket.close();
					} catch (IOException e) {
					}
				}
				if(this.exit) {
					Thread.currentThread().interrupt();
					break;
				}
			}

		}
	}

	private String login(String userName) {
		if(checkLoginStatus(userName)) {
			return "Already logged in.";
		}

		this.server.setLoggedInUser(userName);
		return "!loginSuccessfully logged in.";
	}

	private String logout(String[] arr) {
		if(!checkLoginStatus(arr[1])) {
			return "Not logged in.";
		}

		if(this.server.isRegistered(arr[1])) {
			this.server.removeRegisteredUser(arr[1]);
		}

		this.server.removeLoggedInUser(arr[1]);
		return "Successfully logged out.";
	}

	private String send(String[] arr) {
		if(!checkLoginStatus(arr[1])) {
			return "Not logged in.";
		}

		sender = this.clientName;
		publicMsg = "";

		for(int i = 2; i < arr.length; i++) {
			publicMsg = publicMsg + arr[i] + " ";
		}
		publicMsg = "!send" + arr[1] + ": " + publicMsg;
		this.server.publicMessage();
		return "Public message sent successfully!";
	}

	private String register(String[] arr) {
		if(!checkLoginStatus(arr[1])) {
			return "Not logged in.";
		}

		return this.server.setRegisteredUser(arr[1], arr[2]);
	}

	private String lookup(String[] arr) {

		if(!checkLoginStatus(arr[1])) {
			return "!lookupNot logged in.";
		}
		String splitted [] = arr[2].split("\\.");
		INameserverForChatserver ns = this.server.getRootNS();
		String ausgabe="";
		boolean ok=true;
		try {
			for(int i=splitted.length-1; i>=1; i--){
				if(ns!=null){
					ns = ns.getNameserver(splitted[i]);
				} else {
					ausgabe ="!Server nicht vorhanden.";
					ok=false;
					break;
				}
			}
			if(ok){
				ausgabe = ns.lookup(splitted[0]);
			}

		} catch (RemoteException e) {
			ausgabe = "!lookupWrong username or user not registered.";

		}

		return ausgabe;
	}

	private String decryptAuthMsg(String msg) {
		authCounter++;
		try {

			// Create a cipher with RSA characteristics and decrypt the RSA part of the message
			Cipher cipherIn = Cipher.getInstance("RSA/NONE/OAEPWithSHA256AndMGF1Padding");
			cipherIn.init(Cipher.DECRYPT_MODE, this.server.getPrivateKey());
			byte[] decryptedBMsg = cipherIn.doFinal(Base64.decode(msg.getBytes()));

			// Convert received message to string, for it to be possible to get parts of it
			String decryptedSMsg = new String(decryptedBMsg, "UTF-8");

			// Split the message on whitespaces
			String[] parts = decryptedSMsg.split("\\s");
			this.clientName=parts[1];

			/*-------------------------------------Now response---------------------------------------*/
			// Create a path to client's public key
			
			String path = this.server.getConfig().getString("keys.dir");
			path = path + "/" + parts[1] + ".pub.pem";
			File key = new File(path);
			
			// Read public key of client
			Key clientKey = Keys.readPublicPEM(key);
			
			// Create a cipher with RSA characteristics
			Cipher cipherOut = Cipher.getInstance("RSA/NONE/OAEPWithSHA256AndMGF1Padding");
			cipherOut.init(Cipher.ENCRYPT_MODE, clientKey);

			// Concatenate message with base64 encoded secure number
			ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
			byteStream.write("!ok ".getBytes());
			byteStream.write(parts[2].getBytes());
			byteStream.write(" ".getBytes());
			serverChallenge = getSecureRandom(32);
			byteStream.write(Base64.encode(serverChallenge));
			byteStream.write(" ".getBytes());
			SecretKey AESkey1 = getAESKey();
			byteStream.write(Base64.encode(AESkey1.getEncoded()));
			byteStream.write(" ".getBytes());
			byte[] AESkey2 = getSecureRandom(16);
			byteStream.write(Base64.encode(AESkey2));
			byte[] msgBytes = byteStream.toByteArray();

			byte[] encryptedMsg = cipherOut.doFinal(msgBytes);

			IvParameterSpec AESkey2P = new IvParameterSpec(AESkey2);
			encipher = Cipher.getInstance("AES/CTR/NoPadding");
			decipher = Cipher.getInstance("AES/CTR/NoPadding");
			try {
				encipher.init(Cipher.ENCRYPT_MODE, AESkey1, AESkey2P);
				decipher.init(Cipher.DECRYPT_MODE, AESkey1, AESkey2P);
			} catch (InvalidAlgorithmParameterException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}


			// Finally base64 encode the message before sending
			byte[] base64Msg = Base64.encode(encryptedMsg);

			return new String(base64Msg, "UTF-8");
		} catch (FileNotFoundException e){
			return "User does not exist";
		} catch(NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch(NoSuchPaddingException e) {
			e.printStackTrace();
		} catch(IOException e) {
			e.printStackTrace();
		} catch(InvalidKeyException e) {
			e.printStackTrace();
		} catch(IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch(BadPaddingException e) {
			e.printStackTrace();
		}
		this.auth = true;
		return null;
	}

	private byte[] encryptMsg(String msg) {
		try {
			return Base64.encode(this.encipher.doFinal(msg.getBytes()));
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private byte[] decryptMsg(String msg) {
		try {
			return decipher.doFinal(Base64.decode(msg.getBytes()));
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		}
		return null;
	}

	private byte[] getSecureRandom(int size) {
		final byte[] number = new byte[size];
		secureRandom.nextBytes(number);
		return number;
	}

	private SecretKey getAESKey() {
		SecretKey key = null;
		try {
			KeyGenerator generator = KeyGenerator.getInstance("AES");
			generator.init(256);
			key = generator.generateKey();
		} catch(NoSuchAlgorithmException e) {}
		return key;
	}

	private boolean checkLoginStatus(String user) {
		ArrayList<String> loggedIn = this.server.getLoggedInUsers();
		
		if(!loggedIn.contains(user)) {
			return false;
		}

		return true;
	}

	public void publicMessage() {
		try {
			PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
			writer.println(new String(encryptMsg(publicMsg), "UTF-8"));
		} catch(IOException e) {
		}
	}

	public String getClientName() {
		return this.clientName;
	}

	public String getSender() {
		return sender;
	}

	public void stop() {
		String[] ar = {"",this.clientName};
		this.logout(ar);
		this.writer.println("Server went down. Type >>!exit<< to close the client.");
		try {Thread.sleep(100);} catch(InterruptedException e) {}
		this.exit = true;
	}
}
