package chatserver;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Thread to listen for incoming data packets on the given socket.
 */
public class UDPListenerThread implements Runnable {

    private DatagramSocket datagramSocket;
    private Chatserver server;
    private boolean exit = false;

    public UDPListenerThread(DatagramSocket datagramSocket, Chatserver server) {
        this.datagramSocket = datagramSocket;
        this.server = server;
    }

    public void run() {

        byte[] buffer;
        DatagramPacket packet;
        try {
            while (!exit) {
                buffer = new byte[1024];
                packet = new DatagramPacket(buffer, buffer.length);
                datagramSocket.receive(packet);
                String request = new String(packet.getData());
                String[] parts = request.split("\\s");
                String response = "!error provided message does not fit the expected format: " + "!list";
                
                // check if request has the correct format:
                if (parts.length == 3) {
                    if (parts[1].equals("!list")) {
                        response = createList();
                    }
                }
                
                // get the address of client from the received packet
                InetAddress address = packet.getAddress();

                // get the port of client from the received packet
                int port = packet.getPort();
                buffer = response.getBytes();

                packet = new DatagramPacket(buffer, buffer.length, address, port);

                datagramSocket.send(packet);
            }
        } catch (IOException e) {
	    this.exit = true;
        } finally {
            if (datagramSocket != null && !datagramSocket.isClosed()) {
                datagramSocket.close();
            }
	    if(this.exit) {
                Thread.currentThread().interrupt();
            }
        }

    }

    private String createList() {
        String resp = "Online users:\n";
        ArrayList<String> loggedIn = this.server.getLoggedInUsers();
        for(Iterator<String> it = loggedIn.iterator();it.hasNext();) {
            resp = resp + "* " +  it.next() + "\n";
        }
        return resp;
    }

    public void stop() {
        this.exit = true;
    }
}
