package cli;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;

/**
 * Created by Ivan
 */
public class ShellUsable implements Runnable{

    private Shell shell;
    private String componentName;
    private PrintStream userRespStream;

    public ShellUsable(String componentName, Object c, InputStream userRequestStream, OutputStream userResponseStream){
        shell = new Shell(componentName, userRequestStream, userResponseStream);
        this.componentName = componentName;
        this.userRespStream = new PrintStream(userResponseStream);
        shell.register(c);
    }

    @Override
    public void run(){
        new Thread(shell).start();
        //this.userRespStream.println(this.componentName + "-Client is ready...\n");
    }

    public Shell getShell() {
        return this.shell;
    }

}
