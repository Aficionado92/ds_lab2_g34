package client;

import java.io.*;
import java.net.*;

import cli.*;
import util.Config;
import org.bouncycastle.util.encoders.Base64;
import util.Keys;

import java.security.InvalidKeyException;
import java.security.Key;
import javax.crypto.Mac;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Client implements IClientCli, Runnable {

    private String componentName;
    private Config config;
    private InputStream userRequestStream;
    private PrintStream userResponseStream;

    private ShellUsable shell;
    private PrintWriter toServerTCP;
    private BufferedReader serverResponseTCP;
    private Socket TCPSocket = null;
    private DatagramSocket UDPSocket = null;
    private ServerSocket privateSocket = null;
    private Thread serverThread = null;
    private SecureCommunication secure = null;
    private boolean quit = false;
    private boolean loginStatus = false;
    private boolean stat = false;
    private boolean registered = false;
    private String lstMsg = "";
    public String address = "";
    private String userName="client";
    /**
     * @param componentName
     *            the name of the component - represented in the prompt
     * @param config
     *            the configuration to use
     * @param userRequestStream
     *            the input stream to read user input from
     * @param userResponseStream
     *            the output stream to write the console output to
     */
    public Client(String componentName, Config config, InputStream userRequestStream, PrintStream userResponseStream) {
        this.componentName = componentName;
        this.config = config;
        this.userRequestStream = userRequestStream;
        this.userResponseStream = userResponseStream;
        this.shell = new ShellUsable(componentName, this, userRequestStream, userResponseStream);
    }

    @Override
    public void run() {
        new Thread(this.shell).start();

        try {
            this.TCPSocket = new Socket(this.config.getString("chatserver.host"),this.config.getInt("chatserver.tcp.port"));
            this.toServerTCP = new PrintWriter(this.TCPSocket.getOutputStream(), true);
            this.serverResponseTCP = new BufferedReader(new InputStreamReader(this.TCPSocket.getInputStream()));
            this.secure = new SecureCommunication(this.toServerTCP, this.serverResponseTCP, this);
            new Thread(this.secure).start();
        } catch (UnknownHostException e) {
            this.userResponseStream.println("Cannot connect to host: " + e.getMessage());
        } catch (IOException e) {
            this.userResponseStream.println(e.getClass().getSimpleName() + ": " + e.getMessage());
        }

        try {
            this.UDPSocket = new DatagramSocket();
        } catch (IOException e) {
            this.userResponseStream.println(e.getClass().getSimpleName() + ": " + e.getMessage());
        }

    }

    @Override
    @Command
    public String login(String username, String password) throws IOException {
        return "Command is not supported.";
    }

    @Override
    @Command
    public String logout() throws IOException {
        String msg = "!logout " + this.userName;
	    this.loginStatus = false;
	    this.toServerTCP.println(new String(this.secure.encryptMessage(msg), "UTF-8"));
	    if(this.registered) {
            this.serverThread.interrupt();
            try {
                this.privateSocket.close();
            } catch(IOException e) {}
        }
        return null;
    }

    @Override
    @Command
    public String send(String message) throws IOException {
        String msg = "!send " + this.userName + " " + message;
        this.toServerTCP.println(new String(this.secure.encryptMessage(msg), "UTF-8"));
        return null;
    }

    @Override
    @Command
    public String list() throws IOException {
        try {
            byte[] buffer;
            DatagramPacket packet;
            String msg = this.userName + " !list\n";
            buffer = msg.getBytes();
            packet = new DatagramPacket(buffer,buffer.length, InetAddress.getByName(this.config.getString("chatserver.host")),
                    this.config.getInt("chatserver.udp.port"));
            this.UDPSocket.send(packet);
            buffer = new byte[1024];
            packet = new DatagramPacket(buffer, buffer.length);
            // wait for response from server
            this.UDPSocket.receive(packet);
            this.userResponseStream.println(new String(packet.getData()));
        } catch(UnknownHostException e) {
            this.userResponseStream.println("Cannot connect to host: " + e.getMessage());
        } catch(IOException e) {
            this.userResponseStream.println(e.getClass().getSimpleName() + ": " + e.getMessage());
        }
        return null;
    }

    @Override
    @Command
    public String msg(String username, String message) throws IOException {
        this.stat = true;
        String resp = "Wrong username or user not reachable.";
        this.lookup(username);
        while(this.address.equals("")) {}
        if(!this.address.contains(":")) {
            return null;
        }
        Socket s = null;
        String[] split = this.address.split(":");
        try{
            s = new Socket(split[0].trim(),Integer.parseInt(split[1]));
            BufferedReader privateIn = new BufferedReader(new InputStreamReader(s.getInputStream()));
            PrintWriter privateOut = new PrintWriter(s.getOutputStream(), true);

            byte[] HMAC = this.createHMAC(("!msg " + message).getBytes());
            String msg = new String(HMAC, "UTF-8") + " !msg " + this.userName + " " + message;

            privateOut.println(msg);
            msg = privateIn.readLine();
            String[] parts = msg.split("\\s");
            if(parts[1].equals("!ack") && this.compareHMACs(parts[0].getBytes(),this.createHMAC("!ack".getBytes()))) {
                resp = username + " replied with !ack.";
            } else {
                if(parts[1].equals("!tampered")) {
                    resp = "Your message to " + username + " has benn changed en route!";
                } else {
                    resp = "Answer from " + username + " has been changed en route!";
                }
            }
            this.address = "";
            this.stat = false;
        } catch(IOException e) {
            this.userResponseStream.println("Problem in client -> msg...");
        } finally {
            if (s != null && !s.isClosed()) {
                try {
                    s.close();
                } catch (IOException e) {
                }
            }
        }
        this.userResponseStream.println(resp);
        return null;
    }

    @Override
    @Command
    public String lookup(String username) throws IOException {
        String msg = "!lookup " + this.userName + " " + username;
        this.toServerTCP.println(new String(this.secure.encryptMessage(msg), "UTF-8"));
        return null;
    }

    @Override
    @Command
    public String register(String privateAddress) throws IOException {
        String msg = "!register " + this.userName + " " + privateAddress;
        try {
            String address[] = privateAddress.split(":");
            int a = Integer.parseInt(address[1]);
            this.toServerTCP.println(new String(this.secure.encryptMessage(msg), "UTF-8"));
            this.privateSocket = new ServerSocket(a);
            this.serverThread = new Thread(new ClientListener(this.privateSocket, this));
            this.serverThread.start();
            this.registered = true;
        } catch(Exception e) {
            this.userResponseStream.println("Address to register does not fit the expected format: address:port");
        }
        return null;
    }

    @Override
    @Command
    public String lastMsg() throws IOException {
        if(this.lstMsg.equals("")) {
            return "No message received!\n";
        }
        return this.lstMsg;
    }

    @Override
    @Command
    public String exit() throws IOException {
        if(this.loginStatus) {
            this.logout();
        }
        if(this.registered) {
            this.serverThread.interrupt();
            try {
                this.privateSocket.close();
            } catch(IOException e) {}
        }
        try {
            this.TCPSocket.close();
            this.UDPSocket.close();
        } catch(IOException e) {}
        this.shell.getShell().close();
        this.lstMsg = "";
        this.quit = true;
        return "Client is being terminated.\n";
    }
    
    /**
     * @param args
     *            the first argument is the name of the {@link Client} component
     */
    public static void main(String[] args) {
	    Client client = new Client(args[0], new Config("client"), System.in, System.out);
        new Thread(client).start();
    }

    // --- Commands needed for Lab 2. Please note that you do not have to
    // implement them for the first submission. ---
    //TODO
    @Override
    @Command
    public String authenticate(String username) throws IOException {
        String path = this.config.getString("keys.dir");
        path = path + "/" + username + ".pem";
        File key = new File(path);
        if(!key.exists()) {
            this.userResponseStream.println(username + " does not exists.");
        }else{
        	this.userName=username;
        }
        this.secure.authenticate(username);
        return null;
    }

    /*--------------------------------------------------------------------------------------------------------------
    * --------------------------------------------------------------------------------------------------------------*/
    public PrintStream getUserRespStream() {
        return this.userResponseStream;
    }

    protected void setLstMsg(String msg) {
        this.lstMsg = msg;
    }

    protected Config getConfig() {
        return this.config;
    }

    protected void shutdown() throws IOException {
        try {
            this.TCPSocket.close();
            this.UDPSocket.close();
        } catch(IOException e) {}
    }

    protected byte[] createHMAC(byte[] msg) {
        byte[] hmac;
        File key = new File(this.config.getString("hmac.key"));
        try {
            Key secretKey = Keys.readSecretKey(key);
            Mac hMac = Mac.getInstance("HmacSHA256");
            hMac.init(secretKey);
            hMac.update(msg);
            hmac = hMac.doFinal();
            hmac = this.encodeHMAC(hmac);
            return hmac;
        } catch(IOException e) {
            this.userResponseStream.println("Problem by reading shared key.");
        } catch(NoSuchAlgorithmException e) {
            this.userResponseStream.println("Problem by getting instance of hash algorithm.");
        } catch(InvalidKeyException e) {
            this.userResponseStream.println("Problem by getting using secret key.");
        }
        return null;
    }

    protected byte[] encodeHMAC(byte[] hMac) {
        return Base64.encode(hMac);
    }

    protected byte[] decodeHMAC(byte[] hMac) {
        return Base64.decode(hMac);
    }

    protected boolean compareHMACs(byte[] receivedHMac, byte[] computedHMac) {
        byte[] decodeRecv = this.decodeHMAC(receivedHMac);
        byte[] decodeComp = this.decodeHMAC(computedHMac);
        return MessageDigest.isEqual(decodeRecv, decodeComp);
    }
}
