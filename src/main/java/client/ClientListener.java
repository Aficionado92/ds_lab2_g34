package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Thread to listen for incoming connections on the given socket.
 */
public class ClientListener implements Runnable {

    private ServerSocket sSocket;
    private Client client;
    private boolean listen = true;

    public ClientListener(ServerSocket serverSocket, Client c) {
        this.sSocket = serverSocket;
        this.client = c;
    }

    public void run() {
        while (listen) {
            Socket socket = null;
            try {
                socket = this.sSocket.accept();

                BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

                PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);

                String request;
                // read client requests
                while ((request = reader.readLine()) != null) {
                    String[] parts = request.split("\\s");
                    String response = new String();
                    if(parts[1].equals("!msg")) {
                        String msg = recreateMsg(parts);
                        byte[] hmac = this.client.createHMAC(("!msg " + msg).getBytes());
                        String pMsg = "Private message from " + parts[2] + ": " + msg;
                        if(this.client.compareHMACs(parts[0].getBytes(), hmac)) {
                            this.client.getUserRespStream().println(pMsg);
                            byte[] respHMAC = this.client.createHMAC("!ack".getBytes());
                            response = new String(respHMAC, "UTF-8") + " !ack";
                            // send !ack respond to client
                            writer.println(response);
                            continue;
                        } else {
                            this.client.getUserRespStream().println(pMsg);
                            byte[] respHMAC = this.client.createHMAC(("!tampered " + msg).getBytes());
                            response = new String(respHMAC, "UTF-8") + " !tampered " + msg;
                        }
                    }
                    // send !tampered respond to client
                    writer.println(response);
                }

            } catch (IOException e) {
		        this.listen = false;
                break;
            } finally {
                if (socket != null && !socket.isClosed()) {
                    try {
                        socket.close();
                    } catch (IOException e) {
                    }
                }
		    if(!listen) {
                    Thread.currentThread().interrupt();
                }
            }

        }
    }

    private String recreateMsg(String[] arr) {
        String msg = new String();

        for(int i = 3; i < arr.length; i++) {
            if(i < arr.length - 1) {
                msg = msg + arr[i] + " ";
            } else {
                msg = msg + arr[i];
            }
        }

        return msg;
    }

}
