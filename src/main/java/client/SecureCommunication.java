package client;

import java.io.*;

import org.bouncycastle.util.encoders.Base64;
import util.Keys;

import java.security.*;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;


/**
 * Created by Ivan on 09.01.2017.
 */
public class SecureCommunication implements Runnable {

	private PrintWriter toServer = null;
	private BufferedReader fromServer = null;
	private Client client = null;
	private Key serverKey = null;
	private Key privateKey = null;
	private SecretKey secretKey;
	private IvParameterSpec IV;
	private SecureRandom secureRandom = new SecureRandom();
	private Cipher encryptCipher = null;
	private Cipher decryptCipher = null;

	private String username;
	private boolean quit = false;
	private boolean auth = false;
	byte[] challenge = null;
	String challengeStr=null;

	public SecureCommunication(PrintWriter toServer, BufferedReader fromServer, Client c) {
		this.toServer = toServer;
		this.fromServer = fromServer;
		this.client = c;
	}

	public void run() {
		String msg;
		while(!this.quit) {
			try {
				msg = this.fromServer.readLine();
			
				byte[] msgB = msg.getBytes();

				if(!this.auth) {
					msg = this.decrypt2ndMsg(msgB);
					auth=true;
				} else {
					msg = this.decryptMessage(msgB);
					
					if(msg.startsWith("!send")) {
						msg = msg.replace("!send","");
						this.client.setLstMsg(msg);
						this.client.getUserRespStream().println(msg);
					} else if(msg.startsWith("!lookup")) {
						msg = msg.replace("!lookup","");
						this.client.getUserRespStream().println(msg);
						this.client.address = msg;
					} else if(msg.startsWith("!login")) {
						msg = msg.replace("!login","");
						this.client.getUserRespStream().println(msg);
					} else if(msg.startsWith("Server")) {
						this.client.getUserRespStream().println(msg);
						this.client.shutdown();
					}else if(msg.startsWith("!Failed")) {
						this.client.getUserRespStream().println(msg);
						auth=false;
						this.client.shutdown();
						quit=true;
						
					} else {
						this.client.getUserRespStream().println(msg);
					}
				}

					
				
			} catch (IOException e) {
				this.client.getUserRespStream().println("Client exitted");
				break;
			}
		}
	}


	protected void authenticate(String username) {
		this.username = username;
		String message = "!authenticate " + username + " ";
		// First create path to the server key
		String path = this.client.getConfig().getString("chatserver.key");
		File key = new File(path);
		try {
			// Read server key
			this.serverKey = Keys.readPublicPEM(key);

			// Get random secure number and encode it in base64
			this.challenge = this.getSecureRandom();
			byte[] base64Challenge = Base64.encode(challenge);
			challengeStr=new String(base64Challenge, "UTF-8");

			// Concatenate message with base64 encoded secure number
			ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
			byteStream.write(message.getBytes());
			byteStream.write(base64Challenge);
			byte[] msgBytes = byteStream.toByteArray();

			// Create RSA cipher and encrypt the message
			Cipher cipher = Cipher.getInstance("RSA/NONE/OAEPWithSHA256AndMGF1Padding");
			cipher.init(Cipher.ENCRYPT_MODE, this.serverKey);
			byte[] encryptedMsg = cipher.doFinal(msgBytes);

			// Finally base64 encode the message before sending
			byte[] base64Msg = Base64.encode(encryptedMsg);

			// Send the message to the server bur convert it to String
			this.toServer.println(new String(base64Msg, "UTF-8"));



		} catch(IOException e) {
			this.client.getUserRespStream().println("Problem by reading chatserver public key.");
		} catch(NoSuchAlgorithmException e) {
			this.client.getUserRespStream().println("Problem by selecting of algorithm.");
		} catch(NoSuchPaddingException e) {
			this.client.getUserRespStream().println("Problem by getting instance of cipher.");
		} catch(InvalidKeyException e) {
			this.client.getUserRespStream().println("Problem by using server key.");
		} catch(IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch(BadPaddingException e) {
			e.printStackTrace();
		}


	}

	protected byte[] encryptMessage(String msg) {

		try {
			return Base64.encode(this.encryptCipher.doFinal(msg.getBytes()));
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		}
		return null;
	}

	private String decryptMessage(byte[] msg) {
		try {
			return new String(this.decryptCipher.doFinal(Base64.decode(msg)), "UTF-8");
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return null;
	}

	private String decrypt2ndMsg(byte[] msg) {
		// Decode base64 message received from server
		byte[] decodedMsg = Base64.decode(msg);

		// Create a path to user's private key
		String path = this.client.getConfig().getString("keys.dir");
		path = path + "/" + this.username + ".pem";
		File key = new File(path);
		try {
			// Read private key of client
			this.privateKey = Keys.readPrivatePEM(key);

			// Create a cipher with RSA characteristics and decrypt the RSA part of the message
			Cipher cipher = Cipher.getInstance("RSA/NONE/OAEPWithSHA256AndMGF1Padding");
			cipher.init(Cipher.DECRYPT_MODE, this.privateKey);
			byte[] decryptedBMsg = cipher.doFinal(decodedMsg);

			// Convert received message to string, for it to be possible to get parts of it
			String decryptedSMsg = new String(decryptedBMsg, "UTF-8");

			// Split the message on whitespaces
			String[] parts = decryptedSMsg.split("\\s");

			// Second argument has to be client-challenge and we need to compare it with generated one
			if(parts[1].equals(challengeStr)) {
				byte[] secretK = Base64.decode(parts[3]);
				byte[] IV = Base64.decode(parts[4]);
				this.secretKey = new SecretKeySpec(secretK, 0, secretK.length, "AES");
				this.IV = new IvParameterSpec(IV);

				// Now is time to create AES en- and decrypt ciphers
				this.encryptCipher = Cipher.getInstance("AES/CTR/NoPadding");
				this.encryptCipher.init(Cipher.ENCRYPT_MODE, this.secretKey, this.IV);

				this.decryptCipher = Cipher.getInstance("AES/CTR/NoPadding");
				this.decryptCipher.init(Cipher.DECRYPT_MODE, this.secretKey, this.IV);

				// Now we have to create last authenticate respond
				byte[] encryptedMsg = this.encryptMessage(parts[2]);

				this.toServer.println(new String(encryptedMsg, "UTF-8"));


			} else {
				this.client.getUserRespStream().println("There occured a mismatch by client-challenge comparison.");
			}
		} catch(NoSuchAlgorithmException e) {
			this.client.getUserRespStream().println("Problem by -> cipher.getInstance().");
		} catch(NoSuchPaddingException e) {
			this.client.getUserRespStream().println("Problem by -> cipher.getInstance().");
		} catch(IOException e) {
			try {
				this.client.getUserRespStream().println("Problem by -> Key.read...(). " + new String(msg, "UTF-8"));
				this.quit=true;
			} catch (UnsupportedEncodingException e1) {
			} 
		} catch(InvalidKeyException e) {
			this.client.getUserRespStream().println("Problem by -> cipher.init().");
		} catch(IllegalBlockSizeException e) {
			this.client.getUserRespStream().println("Problem by -> cipher.doFinal().");
		} catch(BadPaddingException e) {
			this.client.getUserRespStream().println("Problem by -> cipher.doFinal().");
		} catch(InvalidAlgorithmParameterException e) {
			this.client.getUserRespStream().println("Problem with IV.");
		}
		return null;
	}

	protected void exit() {
		this.quit = true;
	}

	/*---------------------------------------------------------------------------------------------------------------
	 * -------------------------------------------------------------------------------------------------------------*/
	private byte[] getSecureRandom() {
		final byte[] number = new byte[32];
		this.secureRandom.nextBytes(number);
		return number;
	}

}
