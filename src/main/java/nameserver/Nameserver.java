package nameserver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.logging.Logger;

import nameserver.exceptions.AlreadyRegisteredException;
import nameserver.exceptions.InvalidDomainException;
import util.Config;

public class Nameserver implements INameserverCli, INameserver, INameserverForChatserver, Runnable {

	private String componentName;
	private Config config;
	private InputStream userRequestStream;
	private PrintStream userResponseStream;
	private INameserver rootNameServer;
	private Map<String, INameserver> domains = new HashMap<String, INameserver>();
	private Map<String, String> users = new HashMap<String, String>();
	
	private boolean isRoot=false;
	private String domain="";
	private Registry registry;
	private INameserver remote;
	private final static Logger LOGGER = Logger.getLogger(Nameserver.class.getName());
	
	
	/**
	 * @param componentName
	 *            the name of the component - represented in the prompt
	 * @param config
	 *            the configuration to use
	 * @param userRequestStream
	 *            the input stream to read user input from
	 * @param userResponseStream
	 *            the output stream to write the console output to
	 */
	public Nameserver(String componentName, Config config, InputStream userRequestStream, PrintStream userResponseStream) {
		this.componentName = componentName;
		this.config = config;
		this.userRequestStream = userRequestStream;
		this.userResponseStream = userResponseStream;
		
		try{
			this.domain=this.config.getString("domain");
			LOGGER.info("Started Nameserver "+this.domain);
		}catch(MissingResourceException e){
			isRoot=true;
			LOGGER.info("Started Rootserver");
		}

	}

	@Override
	public void run() {
		if(isRoot){
			try {
				// create and export the registry instance on localhost at the specified port
				registry = LocateRegistry.createRegistry(config.getInt("registry.port"));

				// create a remote object of this server object
				remote = (INameserver) UnicastRemoteObject.exportObject(this, 0);

				// bind the obtained remote object on specified binding name in the registry
				registry.bind(config.getString("root_id"), remote);
				
			} catch (RemoteException e) {
				throw new RuntimeException("Error while starting server.", e);
			} catch (AlreadyBoundException e) {
				throw new RuntimeException("Error while binding remote object to registry.", e);
			}
			
		}else{
			//nameserver
			try {
				// obtain registry that was created by the server
				registry = LocateRegistry.getRegistry(config.getString("registry.host"), config.getInt("registry.port"));

				// look for the bound server remote-object implementing the IServer interface

				rootNameServer = (INameserver) registry.lookup(config.getString("root_id"));

				remote = (INameserver) UnicastRemoteObject.exportObject(this, 0);

				rootNameServer.registerNameserver(this.config.getString("domain"), remote, remote);

			} catch (RemoteException e) {
				throw new RuntimeException("Error while obtaining registry/server-remote-object.", e);
			} catch (NotBoundException e) {
				throw new RuntimeException("Error while looking for server-remote-object.", e);
			} catch (AlreadyRegisteredException e) {
				userResponseStream.println(e.getMessage());
			} catch (InvalidDomainException e) {
				userResponseStream.println(e.getMessage());
			}
		}


		BufferedReader reader = new BufferedReader(new InputStreamReader(
				userRequestStream));
		try {
			// read commands from console
			String input="";
			while((input=reader.readLine())!=null){
				if(input.equals("!nameservers")){
					userResponseStream.println(nameservers());
				}else if(input.equals("!addresses")){
					userResponseStream.println(addresses());
				}else if(input.equals("!exit")){
					userResponseStream.println(exit());
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
			// IOException from System.in is very very unlikely (or impossible)
			// and cannot be handled
		}

	}

	@Override
	public String nameservers() throws IOException {
		int count=1;
		String output="";
		for(String key: domains.keySet()){
			output= output+count++ +" " + key +'\n';
		}
		return output;
	}

	@Override
	public String addresses() throws IOException {
		int count=1;
		String output="";
		for(String key: users.keySet()){
			output= output+count++ +" " + key +" "+ users.get(key)+'\n';
		}
		return output;
	}

	@Override
	public String exit() throws IOException {
		if(isRoot){
			
			if(!UnicastRemoteObject.unexportObject(this,true)){
				return "failed to exit in "+ componentName;
			}
			
			if(!UnicastRemoteObject.unexportObject(registry,true)){
				return "failed to exit in root";
			}
			
		}else{
			if(!UnicastRemoteObject.unexportObject(this,true)){
				return "failed to exit in "+ componentName;
			}
		}
		return "exited...";
		
	}

	/**
	 * @param args
	 *            the first argument is the name of the {@link Nameserver}
	 *            component
	 */
	public static void main(String[] args) {
		Nameserver nameserver = new Nameserver(args[0], new Config(args[0]), System.in, System.out);
		nameserver.run();
	}

	@Override
	public void registerUser(String username, String address)
            throws RemoteException, AlreadyRegisteredException, InvalidDomainException {
		LOGGER.info("Registering User "+username);
		String [] splitted = username.split("\\.");

		//check if the main domain exists
		String userName = splitted[splitted.length-1];

		if(splitted.length==1){

			if(users.containsKey(userName)){
				LOGGER.warning(userName +" user is already registered");
				throw new AlreadyRegisteredException(userName +" user is already registered");

			}else{
				LOGGER.info("registered "+userName);
				users.put(userName, address);
			}
		}else{
			if(domains.containsKey(userName)==false){
				LOGGER.warning("Zone: "+userName +" has never been initially registered");
				throw new InvalidDomainException("Domain: "+userName +" has never been originally registered");
			
			}else{
				String subUserName = "";
				for(int i=0; i<splitted.length-1;i++){
					subUserName = subUserName+splitted[i]+".";
				}
				
				subUserName = subUserName.substring(0, subUserName.length()-1);
				domains.get(userName).registerUser(subUserName, address);
			}
		}

	}

	@Override
	public INameserverForChatserver getNameserver(String zone) throws RemoteException {
		return domains.get(zone);
	}

	@Override
	public String lookup(String username) throws RemoteException {
		return users.get(username);
	}

	@Override
	public void registerNameserver(String domain, INameserver nameserver, INameserverForChatserver nameserverForChatserver)
            throws RemoteException, AlreadyRegisteredException, InvalidDomainException {
		LOGGER.info("Registering Nameserver "+domain);
		String [] splitted = domain.split("\\.");

		//check if the main domain exists
		String mainDomain = splitted[splitted.length-1];

		if(splitted.length==1){
			if(domains.containsKey(mainDomain)){
				LOGGER.warning(mainDomain +" zone is already registered");
				throw new AlreadyRegisteredException(mainDomain +" zone is already registered");
			}else{
				domains.put(mainDomain, nameserver);
				LOGGER.info("Registered zone "+mainDomain);
			}
		}else{
			if(domains.containsKey(mainDomain)==false){
				LOGGER.warning("NS: main domain: "+mainDomain +" has never been registered");
				throw new InvalidDomainException("NS: main domain: "+mainDomain +" has never been registered");
			}else{
				String subDomain = "";
				for(int i=0; i<splitted.length-1;i++){
					subDomain = subDomain+splitted[i]+".";
				}
				subDomain = subDomain.substring(0, subDomain.length()-1);
				domains.get(mainDomain).registerNameserver(subDomain, nameserver, nameserverForChatserver);
			}
		}


	}

}
